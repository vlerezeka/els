<?php

    require './core/Database.php';
    require './controllers/ContentController.php';
    
    $db = new Database;
    
	if(isset($_SESSION['name'])){
        if(!$_SESSION['is_admin'] == true){
            header('Location: login.php');
        }
	} else {
        header('Location: login.php');
    }

    if(isset($_POST['contentadded'])) {
        $content = new ContentController($db);
        $content->saveContent($_POST);
    }


?>


<html>
<head>
    <title>Add Content</title>
    <link rel="stylesheet" type="text/css" href="style.css">
    <script src="jquery-3.4.1.min.js"></script>
    <link href="fontawesome-free-5.9.0-web/css/all.css" rel="stylesheet">
</head>
<body>
<div id="topbuttons">

<?php if(isset($_SESSION['name'])): ?>
    <div><a href="logout.php" title="Logout"><i class="fas fa-sign-out-alt faiconcustom"></i></a></div>
<?php endif; ?>

<?php if(isset($_SESSION['name']) && $_SESSION['is_admin'] == true): ?>
    <div><a href="create-user.php" title="Create user"><i class="fas fa-user-plus faiconcustom"></i></a></div>
    <div><a href="messages.php" title="Messages"><i class="far fa-envelope faiconcustom"></i></a></div>
<?php endif; ?>

</div>


    <div class="sticky-menu left block">
  <nav>
    <span>
      <label for="trigger">☰</label>
      <input type="checkbox" id="trigger">
      <ul>
        <li><a href="index.php">Home</a></li>
        <li><a href="about.php">About</a></li>
        <li><a href="news.php">News</a></li>
        <li><a href="contact.php">Contact</a></li>
      </ul>
    </span>
  </nav>
</div>

    <div id="addcontwrap">
        <h2 id="formtitle">Add content</h2>
        <form action="" method="POST" enctype="multipart/form-data">
        <div class="group">
        <select name="category" id="category">
            <option value="book">Book</option>
            <option value="news">News</option>
        </select>
        </div>
		
        <div class="group">
            <input type="text" name="title"><span class="highlight"></span><span class="bar"></span>
            <label for="title">Title</label>
        </div>
        <div class="group">
            <input type="text" name="shortdescription"><span class="highlight"></span><span class="bar"></span>
            <label for="shortdescription">Short description</label>
        </div>
        <div class="group">
            <textarea name="description" id="" cols="20" rows="3"></textarea><span class="highlight"></span><span class="bar"></span>
            <label for="description">Description</label>
        </div>
        <div class="group">
            <input type="text" name="author"><span class="highlight"></span><span class="bar"></span>
            <label for="author">Author (if category is book)</label>
        </div>
        <div class="group">
            <input type="file" name="imagetoupload" id="imagetoupload">
            <label for="imagepath">Image</label>
        </div>
        <div class="group">
            <input type="file" name="filetoupload" id="filetoupload">
            <label for="filepath">File (if category is book)</label>
        </div>
            <button type="submit" name="contentadded">Add content! <span></span></button>
        </form>
    </div>

    <script>
    $(document).ready(function(){
  $(".selectMenu").click(function(){
    $(this).toggleClass("flip");
  });
  $(".back ul li").click(function(){
    var option = $(this).html();
    $(".front span").html(option);
  });
});
    </script>




</body>
</html>