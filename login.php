<?php
    require './controllers/AuthController.php';
    $auth = new AuthController;

    if(isset($_POST['submitted'])) {
        $auth->login($_POST);
    }
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Login</title>
    <link rel="stylesheet" type="text/css" href="style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
   
</head>
<body>
  
    <div class="wrapper">
        <form method="POST">

            <div class="group">
                <input type="text" name="email" required="required" /><span class="highlight"></span><span class="bar"></span>
                <label>Email</label>
            </div>
            <div class="group">
                <input type="password" name="password" required="required"/><span class="highlight"></span><span class="bar"></span>
                <label>Password</label>
            </div>
            <button type="submit" name="submitted">Login! <span></span></button>
        </form>


    </div>


 <script>
$(function() {
  $("button").on("mouseenter, mouseout", function(e) {
    var elOffset = $(this).offset(),
      dX = e.pageX - elOffset.left,
      dY = e.pageY - elOffset.top;
    $(this)
      .find("span")
      .css({ top: dY, left: dX });
  });
});
</script>


    <!-- <script>
        const el = document.querySelector("body");
		
		document.addEventListener('mousemove', function(e){
			
            el.style.backgroundPositionX = (-e.offsetX / 10) + "px";
            var positionY = 0;
            if ((-e.offsetY + 100) > 0) {
                positionY = 0;
            } else {
                positionY = (-e.offsetY + 100);
            }
            el.style.backgroundPositionY = positionY + "px";
					
		});
        </script> -->
</body>
</html>
