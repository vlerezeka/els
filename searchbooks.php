<?php
	
    require './controllers/UserController.php';
    require './core/Database.php';
    require './controllers/ContentController.php';
    
    $db = new Database;   

    $user = new UserController($db);
	
	if(!isset($_SESSION['name'])){
		header('Location: login.php');
    }
    
    $content = new ContentController($db);
?>
<!DOCTYPE html>
<html>

<head>
    <title>Books</title>
    <link rel="stylesheet" type="text/css" href="style.css">
    <script src="jquery-3.4.1.min.js"></script>
    <script src="snap.svg-min.js"></script>
    <script src="searchbutton.min.js"></script>

  <link href="fontawesome-free-5.9.0-web/css/all.css" rel="stylesheet">
    
 
</head>

<body>

<div id="topbuttons">

<?php if(isset($_SESSION['name'])): ?>
    <div><a href="logout.php" title="Logout"><i class="fas fa-sign-out-alt faiconcustom"></i></a></div>
<?php endif; ?>

<?php if(isset($_SESSION['name']) && $_SESSION['is_admin'] == true): ?>
    <div><a href="create-user.php" title="Create user"><i class="fas fa-user-plus faiconcustom"></i></a></div>
    <div><a href="addcontent.php" title="Add content"><i class="fas fa-plus faiconcustom"></i></a></div>
    <div><a href="messages.php" title="Messages"><i class="far fa-envelope faiconcustom"></i></a></div>
<?php endif; ?>

</div>

<div style="text-align:center;">
  <!--the search bar is initialized by adding jellybo-SearchBar003 class -->
  <div id="searchBtn"></div>
</div>
<div id="wraperi">
    <div class="sticky-menu left block">
        <nav>
            <span>
            <label for="trigger">☰</label>
            <input type="checkbox" id="trigger">
            <ul>
                <li><a href="index.php">Home</a></li>
                <li><a href="about.php">About</a></li>
                <li><a href="news.php">News</a></li>
                <li><a href="contact.php">Contact</a></li>
            </ul>
            </span>
        </nav>
    </div>


    <div id="searchmessage">
    <?php if(isset($_POST['keyword'])): ?>
        <h3> Books found for:  <?php echo $_POST['keyword'] ?></h3> 
        <?php endif; ?>

        <?php if(!isset($_POST['keyword'])): ?>
        <h3> Search for books </h3> 
        <?php endif; ?>
        </div>



	<div id="searchresults"> 
<div class="phpdatabasecontent">
<?php if(isset($_POST['keyword'])): ?>
    <?php $books = $content->searchBooks($_POST['keyword']); ?>    
            <?php foreach($books as $book): ?>
            <div class="contentitem">
            <?php if($book['imagepath'] != ""): ?>
                <div class="box">
                    <img class="contentimage" src="/els/uploads/<?php echo $book['imagepath']; ?> " />    
                </div>
            <?php endif; ?>

                <div class="box">
                    <h1><?php echo $book['title']; ?></h2>
                    <h2><?php echo $book['shortdescription']; ?></h3>
                    <h4><?php echo $book['author']; ?></h3>
                    <?php if($book['filepath'] != ""): ?>
                        <a href="/els/uploads/<?php echo $book['filepath']; ?> ">Download book</a>      
                    <?php endif; ?>
                </div>
            </div>
            <?php endforeach; ?>
<?php endif; ?>
</div>


    </div>




</div>
    
    <!--
			<?php				
				$users = $user->all();
			?>
			<?php foreach($users as $user): ?>
                <div class="box">
                    <h2><?php echo $user['name']; ?></h2>
                    <h3><?php echo $user['email']; ?></h3>
                    <h3><?php echo $user['is_admin']; ?></h3>
               
                </div>
            <?php endforeach; ?>
	-->
	
	
    <!-- <script>
        const el = document.querySelector("body");
		
		document.addEventListener('mousemove', function(e){
			
            el.style.backgroundPositionX = (-e.offsetX / 10) + "px";
            var positionY = 0;
            if ((-e.offsetY + 100) > 0) {
                positionY = 0;
            } else {
                positionY = (-e.offsetY + 100);
            }
            el.style.backgroundPositionY = positionY + "px";
					
		});
        </script> -->
        <script>
$(document).ready(function (){
    $('#searchBtn').jellyboSearchButton003({
        placeholder: "SEARCH",
        onSubmit: onSearchSubmit
    });
    
function onSearchSubmit(event, value) {
    post("/els/searchbooks.php", {keyword: value});
 
 }




 function post(path, params, method='post') {

// The rest of this code assumes you are not using a library.
// It can be made less wordy if you use one.
const form = document.createElement('form');
form.method = method;
form.action = path;

for (const key in params) {
  if (params.hasOwnProperty(key)) {
    const hiddenField = document.createElement('input');
    hiddenField.type = 'hidden';
    hiddenField.name = key;
    hiddenField.value = params[key];

    form.appendChild(hiddenField);
  }
}

document.body.appendChild(form);
form.submit();
}






})
 </script>


</body>

</html>