<?php

// include './core/Database.php';

class MessagesController
{
    protected $db;

    public function __construct($existingdbconn)
    {
        $this->db = $existingdbconn;
    }

    public function all()
    {
        $query = $this->db->pdo->query('SELECT * FROM messages');

        return $query->fetchAll();
    }

    public function send($request)
    {
        $query = $this->db->pdo->prepare('INSERT INTO messages (email, message) VALUES (:email, :message)');
        $query->bindParam(':email', $request['email']);
        $query->bindParam(':message', $request['message']);
        $query->execute();

        return header('Location: ./index.php');
    }
}
