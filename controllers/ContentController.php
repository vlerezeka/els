<?php

// include './core/Database.php';

class ContentController
{
    protected $db;

    // public function __construct()
    // {
    //     $this->db = new Database;
    // }

    public function __construct($existingdbconn)
    {
        $this->db = $existingdbconn;
    }

    public function all()
    {
        $query = $this->db->pdo->query("SELECT * FROM content");

        return $query->fetchAll();
    }

    public function getAllBooks()
    {
        $query = $this->db->pdo->query("SELECT * FROM content WHERE category = 'book'");

        return $query->fetchAll();
    }

    public function getAllNews()
    {
        $query = $this->db->pdo->query("SELECT * FROM content WHERE category = 'news'");

        return $query->fetchAll();
    }

    public function searchBooks($keyword)
    {
        $query = $this->db->pdo->query("SELECT * FROM content WHERE category = 'book' AND (title LIKE '%" . $keyword . "%' OR shortdescription LIKE '%" . $keyword . "%' OR description LIKE '%" . $keyword . "%' OR author LIKE '%" . $keyword . "%')");

        return $query->fetchAll();
    }

    public function searchNews($keyword)
    {
        $query = $this->db->pdo->query("SELECT * FROM content WHERE category = 'news' AND (title LIKE '% . $keyword . %' OR shortdescription LIKE '% . $keyword . %' OR description LIKE '% . $keyword . %')");

        return $query->fetchAll();
    }

    public function saveContent($request)
    {
        $imagepath = $this->saveImage();
        $filepath = $this->saveFile();

        $query = $this->db->pdo->prepare("INSERT INTO content (category, title, author, shortdescription, description, imagepath, filepath, createdby) VALUES (:category, :title, :author, :shortdescription, :description, :imagepath, :filepath, :createdby)");
        $query->bindParam(':category', $request['category']);
        $query->bindParam(':title', $request['title']);        
        $query->bindParam(':author', $request['author']);
        $query->bindParam(':shortdescription', $request['shortdescription']);
        $query->bindParam(':description', $request['description']);
        $query->bindParam(':imagepath', $imagepath);
        $query->bindParam(':filepath', $filepath);
        $query->bindParam(':createdby', $_SESSION['email']);
        $query->execute();

        // return header('Location: ./index.php');
    }

    public function delete($id)
    {
        $query = $this->db->pdo->prepare('DELETE FROM content WHERE id = :id');
        $query->execute(['id' => $id]);

        // return header('Location: ./index.php');
    }


    public function saveImage(){
        if($_FILES["imagetoupload"]) {
            $target_dir = "uploads/";
            $target_file = $target_dir . basename($_FILES["imagetoupload"]["name"]);
            $uploadOk = 1;
            $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
            // Check if image file is a actual image or fake image
            if(isset($_POST["submit"])) {
                $check = getimagesize($_FILES["imagetoupload"]["tmp_name"]);
                if($check !== false) {
                    echo "File is an image - " . $check["mime"] . ".";
                    $uploadOk = 1;
                } else {
                    echo "File is not an image.";
                    $uploadOk = 0;
                }
            }
            // Check if file already exists
            if (file_exists($target_file)) {
                // echo "Sorry, file already exists.";
                return $_FILES["imagetoupload"]["name"];
                $uploadOk = 0;
            }
            // Check file size
            if ($_FILES["imagetoupload"]["size"] > 5000000) {
                echo "Sorry, your file is too large.";
                $uploadOk = 0;
            }
            // Allow certain file formats
            if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif" ) {
                echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                $uploadOk = 0;
            }
            // Check if $uploadOk is set to 0 by an error
            if ($uploadOk == 0) {
                echo "Sorry, your file was not uploaded.";
            // if everything is ok, try to upload file
            } else {
                if (move_uploaded_file($_FILES["imagetoupload"]["tmp_name"], $target_file)) {
                    // echo "The file ". basename( $_FILES["imagetoupload"]["name"]). " has been uploaded.";
                } else {
                    echo "Sorry, there was an error uploading your image.";
                }
            }
            return $_FILES["imagetoupload"]["name"];
        }   
    }
    
    public function saveFile(){
        if($_FILES["filetoupload"]){
            $target_dir = "uploads/";
            $target_file = $target_dir . basename($_FILES["filetoupload"]["name"]);
            $uploadOk = 1;
            $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
            // Check if image file is a actual image or fake image
            // if(isset($_POST["submit"])) {
            //     $check = getimagesize($_FILES["filetoupload"]["tmp_name"]);
            //     if($check !== false) {
            //         echo "File is an image - " . $check["mime"] . ".";
            //         $uploadOk = 1;
            //     } else {
            //         echo "File is not an image.";
            //         $uploadOk = 0;
            //     }
            // }
            // Check if file already exists
            if (file_exists($target_file)) {
                // echo "Sorry, file already exists.";
                return $_FILES["filetoupload"]["name"];
                $uploadOk = 0;
            }
            // Check file size
            if ($_FILES["filetoupload"]["size"] > 9999999999) {
                echo "Sorry, your file is too large.";
                $uploadOk = 0;
            }
            // Allow certain file formats
            if($imageFileType == "jpg" && $imageFileType == "png" && $imageFileType == "jpeg"
            && $imageFileType == "gif" ) {
                echo "Sorry, images cannot be uploaded";
                $uploadOk = 0;
            }
            // Check if $uploadOk is set to 0 by an error
            if ($uploadOk == 0) {
                echo "Sorry, your file was not uploaded.";
            // if everything is ok, try to upload file
            } else {
                if (move_uploaded_file($_FILES["filetoupload"]["tmp_name"], $target_file)) {
                    // echo "The file ". basename( $_FILES["filetoupload"]["name"]). " has been uploaded.";
                } else {
                    echo "Sorry, there was an error uploading your file.";
                }
            }
            return $_FILES["filetoupload"]["name"];
        }
    }



}
