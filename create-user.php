<?php
    require './core/Database.php';
    require './controllers/UserController.php';    
    
    $db = new Database;  

    $user = new UserController($db);
	
	if(isset($_SESSION['is_admin'])){
		if($_SESSION['is_admin'] == false){
			header("Location: index.php");
		}
		
	}
	else {
		header("Location: index.php");
	}

    if(isset($_POST['submitted'])) {
        $user->store($_POST);
    }
?>
<!DOCTYPE html>
<!-- <html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Create User</title>
    <link rel="stylesheet" href="">
</head>
<body>
<div id="topbuttons">

<?php if(isset($_SESSION['name'])): ?>
    <div><a href="logout.php" title="Logout"><i class="fas fa-sign-out-alt faiconcustom"></i></a></div>
<?php endif; ?>

<?php if(isset($_SESSION['name']) && $_SESSION['is_admin'] == true): ?>
    <div><a href="addcontent.php" title="Add content"><i class="fas fa-plus faiconcustom"></i></a></div>
<?php endif; ?>

</div>
    <form action="" method="POST">
        <input type="text" name="fullName">
        <input type="text" name="email">
        <input type="password" name="password">
        <input type="checkbox" name="is_admin">Is Admin?
        <button type="submit" name="submitted">Save</button>
    </form>
</body>
</html> -->





<html>
<head>
    <title>Create user</title>
    <link rel="stylesheet" type="text/css" href="style.css">
    <script src="jquery-3.4.1.min.js"></script>
    <link href="fontawesome-free-5.9.0-web/css/all.css" rel="stylesheet">
</head>
<body>
<div id="topbuttons">

<?php if(isset($_SESSION['name'])): ?>
    <div><a href="logout.php" title="Logout"><i class="fas fa-sign-out-alt faiconcustom"></i></a></div>
<?php endif; ?>

<?php if(isset($_SESSION['name']) && $_SESSION['is_admin'] == true): ?>
    <div><a href="addcontent.php" title="Add content"><i class="fas fa-plus faiconcustom"></i></a></div>
    <div><a href="messages.php" title="Messages"><i class="far fa-envelope faiconcustom"></i></a></div>
<?php endif; ?>

</div>


    <div class="sticky-menu left block">
  <nav>
    <span>
      <label for="trigger">☰</label>
      <input type="checkbox" id="trigger">
      <ul>
        <li><a href="index.php">Home</a></li>
        <li><a href="about.php">About</a></li>
        <li><a href="news.php">News</a></li>
        <li><a href="contact.php">Contact</a></li>
      </ul>
    </span>
  </nav>
</div>

    <div id="addcontwrap">
        <h2 id="formtitle">Create user</h2>
        <form action="" method="POST" enctype="multipart/form-data">
		
        <div class="group">
            <input type="text" name="fullName"><span class="highlight"></span><span class="bar"></span>
            <label for="fullName">Full name</label>
        </div>
        <div class="group">
            <input type="email" name="email"><span class="highlight"></span><span class="bar"></span>
            <label for="email">Email</label>
        </div>
        <div class="group">
            <input type="password" name="password"><span class="highlight"></span><span class="bar"></span>
            <label for="password">Password</label>
        </div>


        <div class="group">
            <input type="checkbox" name="is_admin">
            <label for="is_admin">Is Admin?</label>
        </div>

            <button type="submit" name="submitted">Save<span></span></button>
        </form>
    </div>

    <script>
    $(document).ready(function(){
  $(".selectMenu").click(function(){
    $(this).toggleClass("flip");
  });
  $(".back ul li").click(function(){
    var option = $(this).html();
    $(".front span").html(option);
  });
});
    </script>
