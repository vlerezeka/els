<?php
	require './core/Database.php';
    require './controllers/UserController.php';
    require './controllers/MessagesController.php';
    
    $db = new Database; 

    $user = new UserController($db);
	
	if(!isset($_SESSION['name'])){
		header('Location: login.php');
    }
    
    if(isset($_POST['messagetobesent'])){
      $messages = new MessagesController($db);
      $messages->send($_POST);
    }
?>
<!DOCTYPE html>
<html>

<head>
    <title>Contact us</title>
    <link rel="stylesheet" type="text/css" href="style.css">
    <script src="jquery-3.4.1.min.js"></script>
    <script src="snap.svg-min.js"></script>
    <script src="searchbutton.min.js"></script>

  <link href="fontawesome-free-5.9.0-web/css/all.css" rel="stylesheet">
    
 
</head>

<body>
<div id="topbuttons">

<?php if(isset($_SESSION['name'])): ?>
    <div><a href="logout.php" title="Logout"><i class="fas fa-sign-out-alt faiconcustom"></i></a></div>
<?php endif; ?>

<?php if(isset($_SESSION['name']) && $_SESSION['is_admin'] == true): ?>
    <div><a href="create-user.php" title="Create user"><i class="fas fa-user-plus faiconcustom"></i></a></div>
    <div><a href="addcontent.php" title="Add content"><i class="fas fa-plus faiconcustom"></i></a></div>
<?php endif; ?>

</div>

<div class="sticky-menu left block">
  <nav>
    <span>
      <label for="trigger">☰</label>
      <input type="checkbox" id="trigger">
      <ul>
        <li><a href="index.php">Home</a></li>
        <li><a href="about.php">About</a></li>
        <li><a href="news.php">News</a></li>
        <li><a href="contact.php">Contact</a></li>
      </ul>
    </span>
  </nav>
</div>
<div id="contact-form">
        <h2 id="formtitle">Contact us</h2>
        <form action="" method="POST">
        <div class="group">
            <input name="email" type="text" required="required"/><span class="highlight"></span><span class="bar"></span>
            <label>Email</label>
        </div>
        <div class="group">
            <textarea name="message" type="textarea" rows="5" required="required"></textarea><span class="highlight"></span><span class="bar"></span>
            <label>Message</label>
        </div>
        <button type="submit" name="messagetobesent">Send!<span></span></button>
        </form>
    </div>
</div>


<div class="sticky-menu left block">
  <nav>
    <span>
      <label for="trigger">☰</label>
      <input type="checkbox" id="trigger">
      <ul>
        <li><a href="index.php">Home</a></li>
        <li><a href="about.php">About</a></li>
        <li><a href="#">News</a></li>
        <li><a href="contact.php">Contact</a></li>
      </ul>
    </span>
  </nav>
	
    
    <!--
			<?php				
				$users = $user->all();
			?>
			<?php foreach($users as $user): ?>
                <div class="box">
                    <h2><?php echo $user['name']; ?></h2>
                    <h3><?php echo $user['email']; ?></h3>
                    <h3><?php echo $user['is_admin']; ?></h3>
               
                </div>
            <?php endforeach; ?>
	-->
	
    
   
 <script>

jQuery(function() {
  jQuery("#position").on("click", function() {
    jQuery( ".sticky-menu" ).toggleClass(function() {
      if ( jQuery( this ).parent().is( ".inline" ) ) {
        return "block";
      } else {
        return "inline";
    }
    });
    return false;
  });
  
  jQuery("#align").on("click", function() {
    jQuery( ".sticky-menu" ).toggleClass(function() {
      if ( jQuery( this ).parent().is( ".left" ) ) {
        return "right";
      } else {
        return "left";
    }
    });
    return false;
  });
  
  
});

    </script>


</body>

</html>