<?php
	
    require './controllers/UserController.php';
    require './core/Database.php';
    require './controllers/MessagesController.php';
    
    $db = new Database;   

    $user = new UserController($db);
	
	if(!isset($_SESSION['name'])){
		header('Location: login.php');
    }
    
    $messagescontroller = new MessagesController($db);
?>
<!DOCTYPE html>
<html>

<head>
    <title>News</title>
    <link rel="stylesheet" type="text/css" href="style.css">
    <script src="jquery-3.4.1.min.js"></script>
    <script src="snap.svg-min.js"></script>
    <script src="searchbutton.min.js"></script>

  <link href="fontawesome-free-5.9.0-web/css/all.css" rel="stylesheet">
    
 
</head>

<body>

<div id="topbuttons">

<?php if(isset($_SESSION['name'])): ?>
    <div><a href="logout.php" title="Logout"><i class="fas fa-sign-out-alt faiconcustom"></i></a></div>
<?php endif; ?>

<?php if(isset($_SESSION['name']) && $_SESSION['is_admin'] == true): ?>
    <div><a href="create-user.php" title="Create user"><i class="fas fa-user-plus faiconcustom"></i></a></div>
    <div><a href="addcontent.php" title="Add content"><i class="fas fa-plus faiconcustom"></i></a></div>
<?php endif; ?>

</div>

<div id="wraperi">
    <div class="sticky-menu left block">
        <nav>
            <span>
            <label for="trigger">☰</label>
            <input type="checkbox" id="trigger">
            <ul>
                <li><a href="index.php">Home</a></li>
                <li><a href="about.php">About</a></li>
                <li><a href="news.php">News</a></li>
                <li><a href="contact.php">Contact</a></li>
            </ul>
            </span>
        </nav>
    </div>


    <div id="searchmessage">
        <h3>Messages received</h3> 
    </div>



	<div id="searchresults"> 
<div class="phpdatabasecontent">
    <?php $allmessages = $messagescontroller->all(); ?>    
            <?php foreach($allmessages as $message): ?>
            <div class="contentitem">
            

                <div class="box">
                    <h2><?php echo $message['email']; ?></h2>
                    <h4><?php echo $message['message']; ?></h4>
                </div>



            </div>
            <?php endforeach; ?>
</div>


    </div>




</div>
    
        <script>
$(document).ready(function (){
    $('#searchBtn').jellyboSearchButton003({
        placeholder: "SEARCH",
        onSubmit: onSearchSubmit
    });
    
function onSearchSubmit(event, value) {
    post("/els/searchbooks.php", {keyword: value});
 
 }




 function post(path, params, method='post') {

// The rest of this code assumes you are not using a library.
// It can be made less wordy if you use one.
const form = document.createElement('form');
form.method = method;
form.action = path;

for (const key in params) {
  if (params.hasOwnProperty(key)) {
    const hiddenField = document.createElement('input');
    hiddenField.type = 'hidden';
    hiddenField.name = key;
    hiddenField.value = params[key];

    form.appendChild(hiddenField);
  }
}

document.body.appendChild(form);
form.submit();
}






})
 </script>


</body>

</html>