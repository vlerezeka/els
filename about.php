<?php
	
    require './controllers/UserController.php';
    require './core/Database.php';
    require './controllers/ContentController.php';
    
    $db = new Database;   

    $user = new UserController($db);
	
	if(!isset($_SESSION['name'])){
		header('Location: login.php');
    }
    
    $content = new ContentController($db);
?>
<!DOCTYPE html>
<html>

<head>
    <title>About</title>
    <link rel="stylesheet" type="text/css" href="style.css">
    <script src="jquery-3.4.1.min.js"></script>
    <script src="snap.svg-min.js"></script>
    <script src="searchbutton.min.js"></script>

  <link href="fontawesome-free-5.9.0-web/css/all.css" rel="stylesheet">
    
 
</head>

<body>

<div id="topbuttons">

<?php if(isset($_SESSION['name'])): ?>
    <div><a href="logout.php" title="Logout"><i class="fas fa-sign-out-alt faiconcustom"></i></a></div>
<?php endif; ?>

<?php if(isset($_SESSION['name']) && $_SESSION['is_admin'] == true): ?>
    <div><a href="create-user.php" title="Create user"><i class="fas fa-user-plus faiconcustom"></i></a></div>
    <div><a href="addcontent.php" title="Add content"><i class="fas fa-plus faiconcustom"></i></a></div>
    <div><a href="messages.php" title="Messages"><i class="far fa-envelope faiconcustom"></i></a></div>
<?php endif; ?>

</div>

<div id="wraperi">
    <div class="sticky-menu left block">
        <nav>
            <span>
            <label for="trigger">☰</label>
            <input type="checkbox" id="trigger">
            <ul>
                <li><a href="index.php">Home</a></li>
                <li><a href="about.php">About</a></li>
                <li><a href="news.php">News</a></li>
                <li><a href="contact.php">Contact</a></li>
            </ul>
            </span>
        </nav>
    </div>
	<div id="about">
        <h3> About Us </h3> 
        <p>ELS eshte nje platforme qe i mundeson studenteve te UBT-se te downloadojne libra
        qe gjenden ne biblioteken e UBT-se a naj sen ku e di.ELS eshte nje platforme qe i mundeson studenteve te UBT-se te downloadojne libra
        qe gjenden ne biblioteken e UBT-se a naj sen ku e di. ELS eshte nje platforme qe i mundeson studenteve te UBT-se te downloadojne libra
        qe gjenden ne biblioteken e UBT-se a naj sen ku e di. ELS eshte nje platforme qe i mundeson studenteve te UBT-se te downloadojne libra
        qe gjenden ne biblioteken e UBT-se a naj sen ku e di</p>
    </div>




</div>
	
	
    <!-- <script>
        const el = document.querySelector("body");
		
		document.addEventListener('mousemove', function(e){
			
            el.style.backgroundPositionX = (-e.offsetX / 10) + "px";
            var positionY = 0;
            if ((-e.offsetY + 100) > 0) {
                positionY = 0;
            } else {
                positionY = (-e.offsetY + 100);
            }
            el.style.backgroundPositionY = positionY + "px";
					
		});
        </script> -->
        <script>
$(document).ready(function (){
    $('#searchBtn1').jellyboSearchButton003();


		var db = [];
function onChange(e, value) {
  // get animation object
     var searchBtn = $(this).jellyboSearchButton003();
     db.push(value);
     if (db.length > 3) {
          db.shift();
      }
      if(value.length < 1){
           db = [];
      }
  //first empty suggestion list
      searchBtn.emptyList();
      for(var i in db){
        // added new suggestions
           searchBtn.addListItem(db[i]);
      }
 }
})
 </script>


</body>

</html>